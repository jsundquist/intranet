<?php
namespace Customer;

use Customer\Model\Customer;
use Customer\Model\CustomerTable;
use Customer\Model\Location;
use Customer\Model\LocationTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module {
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Customer\Model\CustomerTable' =>  function($sm) {
                        $tableGateway = $sm->get('CustomerTableGateway');
                        $table = new CustomerTable($tableGateway);
                        return $table;
                    },
                'CustomerTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new Customer());
                        return new TableGateway('customers', $dbAdapter, null, $resultSetPrototype);
                    },
                'Customer\Model\LocationTable' =>  function($sm) {
                        $tableGateway = $sm->get('LocationTableGateway');
                        $table = new LocationTable($tableGateway);
                        return $table;
                    },
                'LocationTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new Location());
                        return new TableGateway('locations', $dbAdapter, null, $resultSetPrototype);
                    },
            ),
        );
    }
} 