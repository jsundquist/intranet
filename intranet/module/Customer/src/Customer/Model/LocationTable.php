<?php
namespace Customer\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class locationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = false)
    {
        if ($paginated) {
            // create a new Select object for the table album
            $select = new Select('locations');
            // create a new result set based on the Album entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Location());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getLocation($id = null)
    {
        $id = (int)$id;
        $rowSet = $this->tableGateway->select(array('id' => $id));
        $row = $rowSet->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveLocation(Location $location)
    {
        $data = array(
            'customerId' => $location->customerId,
            'name' => $location->name,
            'address' => $location->address,
            'address2' => $location->address2,
            'city' => $location->city,
            'state' => $location->state,
            'postalCode' => $location->postalCode,
            'country' => $location->country,
            'phone' => $location->phone,
            'fax' => $location->fax
        );

        $id = (int)$location->id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getLocation($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception("Form id does not exist");
            }
        }
    }

    public function deleteLocation($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
} 