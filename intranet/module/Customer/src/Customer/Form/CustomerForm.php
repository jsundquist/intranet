<?php

namespace Customer\Form;

use Zend\Form\Form;

class CustomerForm extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('customer');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Address Line 1',
            ),
        ));

        $this->add(array(
            'name' => 'address2',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Address Line 2',
            ),
        ));

        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'City',
            ),
        ));

        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'State',
            ),
        ));

        $this->add(array(
            'name' => 'postalCode',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Postal Code',
            ),
        ));

        $this->add(array(
            'name' => 'country',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Country',
            ),
        ));

        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Phone Number',
            ),
        ));

        $this->add(array(
            'name' => 'fax',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Fax Number',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
} 