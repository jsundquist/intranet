<?php
namespace Customer\Controller;

use Customer\Form\LocationForm;
use Customer\Model\Customer;
use Customer\Model\Location;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class LocationController extends AbstractActionController
{
    private $customerTable = null;
    private $locationTable = null;

    public function indexAction()
    {

        $customer = (int)$this->params()->fromRoute('customer', 0);
        if (!$customer) {
            return $this->redirect()->toRoute('customer');
        }

        // grab the paginator from the ProductTable
        $paginator = $this->getLocationTable()->fetchAll(true);
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(10);

        return new ViewModel(array(
            'paginator' => $paginator,
            'customer' => $customer
        ));
    }

    public function addAction()
    {

        $customer = (int)$this->params()->fromRoute('customer', 0);

        if (!$customer) {
            return $this->redirect()->toRoute('customer');
        }

        $form = new LocationForm();
        $form->get('submit')->setValue('Add');
        $form->get('customerId')->setValue($customer);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $location = new Location();
            $form->setInputFilter($location->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $location->exchangeArray($form->getData());
                $this->getLocationTable()->saveLocation($location);

                // Redirect to list of customers
                return $this->redirect()->toRoute('location', array('customer' => $location->customerId));
            }
        }
        return array(
            'form' => $form,
            'customer' => $customer
        );
    }

    public function editAction()
    {

        $customer = (int)$this->params()->fromRoute('customer', 0);

        if (!$customer) {
            return $this->redirect()->toRoute('customer');
        }

        $id = (int)$this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('location', array(
                'customer' => $customer,
                'action' => 'add'
            ));
        }

        $location = $this->getLocationTable()->getLocation($id);

        $form = new LocationForm();
        $form->bind($location);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($location->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getLocationTable()->saveLocation($form->getData());

                // Redirect to list of customers
                return $this->redirect()->toRoute('location', array('customer' => $location->customerId));
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
            'customer' => $customer
        );
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('customer');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int)$request->getPost('id');
                $this->getLocationTable()->deleteLocation($id);
            }

            // Redirect to list of customers
            return $this->redirect()->toRoute('customer');
        }

        return array(
            'id' => $id,
            'customer' => $this->getLocationTable()->getLocation($id)
        );

    }

    public function getCustomerTable()
    {
        if (!$this->customerTable) {
            $sm = $this->getServiceLocator();
            $this->customerTable = $sm->get('Customer\Model\CustomerTable');
        }
        return $this->customerTable;
    }


    public function getLocationTable()
    {
        if (!$this->locationTable) {
            $sm = $this->getServiceLocator();
            $this->locationTable = $sm->get('Customer\Model\LocationTable');
        }
        return $this->locationTable;
    }
} 