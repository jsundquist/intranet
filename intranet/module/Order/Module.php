<?php
namespace Order;

use Order\Model\Order;
use Order\Model\OrderLine;
use Order\Model\OrderLineTable;
use Order\Model\OrderTable;
use Order\Model\ShippingMethod;
use Order\Model\ShippingMethodTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module {

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Order\Model\OrderTable' =>  function($sm) {
                        $tableGateway = $sm->get('OrderTableGateway');
                        $table = new OrderTable($tableGateway);
                        return $table;
                    },
                'OrderTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new Order());
                        return new TableGateway('orders', $dbAdapter, null, $resultSetPrototype);
                    },
                'Order\Model\ShippingMethodTable' =>  function($sm) {
                        $tableGateway = $sm->get('ShippingMethodTableGateway');
                        $table = new ShippingMethodTable($tableGateway);
                        return $table;
                    },
                'ShippingMethodTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new ShippingMethod());
                        return new TableGateway('shipping_methods', $dbAdapter, null, $resultSetPrototype);
                    },
                'Order\Model\OrderLineTable' =>  function($sm) {
                        $tableGateway = $sm->get('OrderLineTableGateway');
                        $table = new OrderLineTable($tableGateway);
                        return $table;
                    },
                'OrderLineTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype(new OrderLine());
                        return new TableGateway('order_lines', $dbAdapter, null, $resultSetPrototype);
                    },
            )
        );
    }
} 