<?php
namespace Order\Form;

use Zend\Form\Form;

class OrderLineForm extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('order');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'orderId',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'productId',
            'options' => array(
                'label' => 'Product',
                'value_options' => array()
            )
        ));

        $this->add(array(
            'name' => 'serialIn',
            'options' => array(
                'label' => 'Serial In'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'problemReported',
            'options' => array(
                'label' => 'Problem Reported',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'reportedDoa',
            'options' => array(
                'label' => 'Reported DDA',
                'use_hidden_element' => true,
                'checked_value' => 'true',
                'unchecked_value' => 'false'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
} 