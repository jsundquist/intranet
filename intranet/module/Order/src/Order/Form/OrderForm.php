<?php
namespace Order\Form;


use Zend\Form\Form;

class OrderForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('order');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'type',
            'options' => array(
                'label' => 'Order Type',
                'value_options' => array(
                    'Standard Repair' => 'Standard Repair'
                )
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'locationId',
            'options' => array(
                'label' => 'Location',
                'value_options' => array(
                    'Standard Repair' => 'Standard Repair'
                )
            )
        ));

        $this->add(array(
            'name' => 'contact',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Contact',
            ),
        ));

        $this->add(array(
            'name' => 'customerPurchaseOrder',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Customer Purchase Order #',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shippingMethodId',
            'options' => array(
                'label' => 'Shipping Method',
                'value_options' => array(
                    'Standard Repair' => 'Standard Repair'
                )
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
} 