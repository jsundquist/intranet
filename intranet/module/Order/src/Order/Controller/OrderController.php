<?php
namespace Order\Controller;


use Order\Form\OrderForm;
use Order\Form\OrderLineForm;
use Order\Model\Order;
use Order\Model\OrderLine;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\View;

class OrderController extends AbstractActionController
{

    private $orderTable;
    private $locationTable;
    private $shippingMethodTable;
    private $orderLineTable;
    private $productTable;

    public function indexAction()
    {
        // Allow for search of work orders based on customer order number, rma number, order date, location, order id

        // grab the paginator from the ProductTable
        $paginator = $this->getOrderTable()->fetchAll(true);
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(10);

        return new ViewModel(array(
            'paginator' => $paginator
        ));
    }

    public function createOrderAction()
    {
        // Get step
        $step = (int) $this->params()->fromRoute('step',1);
// Get order id
        $id = (int)$this->params()->fromRoute('id', '');

        if ($step > 1 && !$id) {
            return $this->redirect()->toRoute('order/createOrder/0/1');
        }

        $request = $this->getRequest();
        // if step 1
        if($step == 1) {
            // if post
            if ($request->isPost()) {
                $params = $this->params()->fromPost();

                // Save location, customer purchase order number, sales order number, contact, shipping method, order type, order date
                $order = new Order();
                $order->exchangeArray($params);
                $id = $this->getOrderTable()->saveOrder($order);

                // redirect to step 2
                return $this->redirect()->toUrl("/order/createOrder/{$id}/2");
            }

            // Get location
            $locations = $this->getLocationTable()->fetchAll(false);

            // Get Shipping Methods
            $shippingMethods = $this->getShippingMethodTable()->fetchAll();

            $viewModel = new ViewModel(array('locations' => $locations, 'shippingMethods' => $shippingMethods));
            $viewModel->setTemplate('order/order/create/step1');
            return $viewModel;
        }

        // if step 2
        if ($step == 2){
            // if post
            if ($request->isPost()) {
                // add number of products to order lines,
                $params = $this->params()->fromPost();
                $quantity = $params['quantity'];
                $productId = $params['partNumberId'];
                for ($i=1;$i<=$quantity;$i++) {
                    $orderLines = new OrderLine();
                    $orderLines->orderId = $params['orderId'];
                    $orderLines->productId = $productId;
                    $this->getOrderLineTable()->saveOrderLine($orderLines);
                }
            }

            // get products
            $products = $this->getProductTable()->fetchAll(false);

            // get order header
            $orderHeader = $this->getOrderTable()->getOrder($id);

            // get current items associated with current order
            $orderLines = $this->getOrderLineTable()->getGroupedOrderLines($id);

            $viewModel = new ViewModel(array('orderHeader' => $orderHeader, 'products' => $products, 'orderLines' => $orderLines));
            $viewModel->setTemplate('order/order/create/step2');
            return $viewModel;
        }

        // if step 3
        if ($step == 3) {
            // if post

            // get order lines
            $orderLines = $this->getOrderLineTable()->getOrderLines($id);

            if($request->isPost()){
                // save serial numbers, problem reported.
                $postParams = $this->params()->fromPost();

                foreach ($orderLines as $i => $orderLine) {
                    $orderLine->serialIn = $postParams['serialNumber'][$i];
                    $orderLine->problemReported = $postParams['problemReported'][$i];
                    $orderLine->orderId = $id;

                    if (!$orderLine->receiveDate) {
                        $orderLine->receiveDate = date('Y-m-d');
                    }

                    $this->getOrderLineTable()->saveOrderLine($orderLine);
                }
                // redirect to step 4
//                $this->redirect()->toUrl("/order/view/{$id}");
            }

            // get order header
            $orderHeader = $this->getOrderTable()->getOrder($id);

            $orderLines = $this->getOrderLineTable()->getOrderLines($id);

            $viewModel = new ViewModel(array('orderHeader' => $orderHeader, 'orderLines' => $orderLines));
            $viewModel->setTemplate('order/order/create/step3');
            return $viewModel;
        }

        return true;
    }

    public function viewAction()
    {
        $id = (int)$this->params()->fromRoute('id', '');
        if (!$id) {
            return $this->redirect()->toRoute('order');
        }

        // get order header
        $orderHeader = $this->getOrderTable()->getOrder($id);

        // get order lines
        $orderLines = $this->getOrderLineTable()->getOrderLines($id);

        return new ViewModel(array('orderHeader' => $orderHeader, 'orderLines' => $orderLines));
    }

    public function editLinesAction()
    {
        $id = (int)$this->params()->fromRoute('id', '');
        if (!$id) {
            return $this->redirect()->toRoute('order');
        }

        // get order header

        // if post
            // save updates

        // get order lines
    }

    public function shipOrderAction()
    {
        $id = (int)$this->params()->fromRoute('id', '');
        if (!$id) {
            return $this->redirect()->toRoute('order');
        }

        // get order header

        // if post
            // add shipping information to order lines
            // redirect to work order

        // get order lines
    }

    public function getOrderTable()
    {
        if (!$this->orderTable) {
            $sm = $this->getServiceLocator();
            $this->orderTable = $sm->get('Order\Model\OrderTable');
        }

        return $this->orderTable;
    }

    public function getOrderLineTable()
    {
        if (!$this->orderLineTable) {
            $sm = $this->getServiceLocator();
            $this->orderLineTable = $sm->get('Order\Model\OrderLineTable');
        }

        return $this->orderLineTable;
    }

    public function getLocationTable()
    {
        if (!$this->locationTable) {
            $sm = $this->getServiceLocator();
            $this->locationTable = $sm->get('Customer\Model\LocationTable');
        }

        return $this->locationTable;
    }

    public function getShippingMethodTable()
    {
        if (!$this->shippingMethodTable) {
            $sm = $this->getServiceLocator();
            $this->shippingMethodTable = $sm->get('Order\Model\ShippingMethodTable');
        }

        return $this->shippingMethodTable;
    }

    public function getProductTable()
    {
        if (!$this->productTable) {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Product\Model\ProductTable');
        }

        return $this->productTable;
    }

    /*
    public function addAction()
    {
        $form = new OrderForm();
        $form->get('submit')->setValue('Add');

        $shippingMethods = $this->getShippingMethodTable()->fetchAll();
        $shippingMethodOptions = array();
        foreach ($shippingMethods as $shippingMethod) {
            $shippingMethodOptions[$shippingMethod->id] = $shippingMethod->method;
        }

        $locations = $this->getLocationTable()->fetchAll(false);
        $locationOptions = array();
        foreach ($locations as $location) {
            $locationOptions[$location->id] = $location->name . ' ' . $location->postalCode;
        }

        $form->get('shippingMethodId')->setValueOptions($shippingMethodOptions);

        $form->get('locationId')->setValueOptions($locationOptions);


        if ($request->isPost()) {
            $order = new Order();
            $form->setInputFilter($order->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $order->exchangeArray($form->getData());
                $this->getOrderTable()->saveOrder($order);
                // Redirect to list of customers
                return $this->redirect()->toRoute('order');
            }
        }
        return array('form' => $form);
    }

    public function viewAction()
    {
        $id = (int)$this->params()->fromRoute('id', '');
        if (!$id) {
            return $this->redirect()->toRoute('order');
        }

        $orderHeader = $this->getOrderTable()->getOrder($id);

        $orderLines = $this->getOrderLineTable()->getOrderLines($id);

        return new ViewModel(array('orderHeader' => $orderHeader, 'orderLines' => $orderLines));
    }

    public function addLinesAction()
    {
        $id = (int)$this->params()->fromRoute('id', '');
        if (!$id) {
            return $this->redirect()->toRoute('order');
        }

        $orderHeader = $this->getOrderTable()->getOrder($id);
        $orderLines = $this->getOrderLineTable()->getOrderLines($id);

        $form = new OrderLineForm();
        $form->get('submit')->setValue('Add Line');

        $products = $this->getProductTable()->fetchAll(false);
        $productOptions = array();
        foreach ($products as $product) {
            $productOptions[$product->id] = $product->partNumber . ' - ' . $product->description;
        }

        $form->get('productId')->setValueOptions($productOptions);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $orderLine = new OrderLine();
            $form->setInputFilter($orderLine->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $orderLine->exchangeArray($form->getData());
                $this->getOrderLineTable()->saveOrderLine($orderLine);

                $this->redirect()->toUrl('/order/addLines/' . $id);
            }
        }

        return array('form' => $form, 'orderHeader' => $orderHeader, 'orderLines' => $orderLines);
    }

    public function editLinesAction()
    {
        $id = (int)$this->params()->fromRoute('id', '');
        if (!$id) {
            return $this->redirect()->toRoute('order');
        }

        $orderHeader = $this->getOrderTable()->getOrder($id);
        $orderLines = $this->getOrderLineTable()->getOrderLines($id);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->redirect()->toUrl('/order/index/' . $id);
        }


        return new ViewModel(array('orderHeader' => $orderHeader, 'orderLines' => $orderLines));
    }

    public function notesAction()
    {
        return new ViewModel();
    }


*/
}