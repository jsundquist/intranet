<?php
namespace Order\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class OrderTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = false)
    {
        if ($paginated) {
            // create a new Select object for the table album
            $select = new Select();
            $select->from('orders')->columns(array('customerPurchaseOrder', 'shippingMethodId', 'contact', 'created', 'locationId', 'id'))->join('locations', 'locations.id = orders.locationId', array('locationName' => 'name', 'locationAddress' => 'address', 'locationAddress2' => 'address2', 'locationCity' => 'city', 'locationState' => 'state', 'locationPostalCode' => 'postalCode'), Select::JOIN_LEFT);
            // create a new result set based on the Album entity
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Order());
            // create a new pagination adapter object
            $paginatorAdapter = new DbSelect(
            // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getOrder($id = null)
    {
        $id = (int)$id;
        $select = new Select();
        $select->from('orders')->columns(array('customerPurchaseOrder', 'shippingMethodId', 'contact', 'created', 'locationId', 'id'))->join('locations', 'locations.id = orders.locationId', array('locationName' => 'name', 'locationAddress' => 'address', 'locationAddress2' => 'address2', 'locationCity' => 'city', 'locationState' => 'state', 'locationPostalCode' => 'postalCode'), Select::JOIN_LEFT)
            ->where(array('orders.id' => $id));
        $rowSet = $this->tableGateway->selectWith($select);
        $row = $rowSet->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveOrder(Order $order)
    {
        $data = array(
            'locationId' => $order->locationId,
            'customerPurchaseOrder' => $order->customerPurchaseOrder,
            'shippingMethodId' => $order->shippingMethodId,
            'contact' => $order->contact,
            'created' => date('Y-m-d H:i:s')
        );

        $id = (int)$order->id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();

            return $id;
        } else {
            if ($this->getOrder($id)) {
                unset($data['created']);
                $this->tableGateway->update($data, array('id' => $id));

                return $id;
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteOrder($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }

} 