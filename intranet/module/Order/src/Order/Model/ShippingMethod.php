<?php
namespace Order\Model;


class ShippingMethod {
    public $id;
    public $method;

    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->method = (isset($data['method'])) ? $data['method'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
} 