<?php
namespace Order\Model;


use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class OrderLine
{
    public $id;
    public $orderId;
    public $quantity;
    public $productId;
    public $partNumber;
    public $alternativePartNumber;
    public $productDescription;
    public $serialIn;
    public $serialOut;
    public $problemReported;
    public $problemFound;
    public $workPreformed;
    public $receivedDate;
    public $shippedDate;
    public $reportedDoa;
    public $foundDoa;
    public $nri;
    public $status;
    public $shippingMethodOut;
    public $shippingMethodIn;
    public $trackingNumberOut;
    public $trackingNumberIn;

    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->orderId = (isset($data['orderId'])) ? $data['orderId'] : null;
        $this->quantity = (isset($data['quantity'])) ? $data['quantity'] : null;
        $this->productId = (isset($data['productId'])) ? $data['productId'] : null;
        $this->partNumber = (isset($data['partNumber'])) ? $data['partNumber'] : null;
        $this->alternativePartNumber = (isset($data['alternativePartNumber'])) ? $data['alternativePartNumber'] : null;
        $this->productDescription = (isset($data['productDescription'])) ? $data['productDescription'] : null;
        $this->serialIn = (isset($data['serialIn'])) ? $data['serialIn'] : null;
        $this->serialOut = (isset($data['serialOut'])) ? $data['serialOut'] : null;
        $this->problemReported = (isset($data['problemReported'])) ? $data['problemReported'] : null;
        $this->problemFound = (isset($data['problemFound'])) ? $data['problemFound'] : null;
        $this->workPreformed = (isset($data['workPreformed'])) ? $data['workPreformed'] : null;
        $this->receivedDate = (isset($data['receivedDate'])) ? $data['receivedDate'] : null;
        $this->shippedDate = (isset($data['shippedDate'])) ? $data['shippedDate'] : null;
        $this->reportedDoa = (isset($data['reportedDoa'])) ? $data['reportedDoa'] : null;
        $this->foundDoa = (isset($data['foundDoa'])) ? $data['foundDoa'] : null;
        $this->nri = (isset($data['nri'])) ? $data['nri'] : null;
        $this->status = (isset($data['status'])) ? $data['status'] : null;
        $this->shippingMethodIn = (isset($data['shippingMethodIn'])) ? $data['shippingMethodIn'] : null;
        $this->shippingMethodOut = (isset($data['shippingMethodOut'])) ? $data['shippingMethodOut'] : null;
        $this->trackingNumberIn = (isset($data['trackingNumberIn'])) ? $data['trackingNumberIn'] : null;
        $this->trackingNumberOut = (isset($data['trackingNumberOut'])) ? $data['trackingNumberOut'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
} 