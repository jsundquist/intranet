<?php
namespace Order\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Order
{

    public $id;
    public $customerPurchaseOrder;
    public $shippingMethodId;
    public $locationId;
    public $contact;
    public $created;

    public $locationName;
    public $locationAddress;
    public $locationAddress2;
    public $locationCity;
    public $locationState;
    public $locationPostalCode;

    public $shippingMethodName;

    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->customerPurchaseOrder = (isset($data['customerPurchaseOrder'])) ? $data['customerPurchaseOrder'] : null;
        $this->shippingMethodId = (isset($data['shippingMethodId'])) ? $data['shippingMethodId'] : null;
        $this->locationId = (isset($data['locationId'])) ? $data['locationId'] : null;
        $this->contact = (isset($data['contact'])) ? $data['contact'] : null;
        $this->created = (isset($data['created'])) ? $data['created'] : null;

        $this->locationName = (isset($data['locationName'])) ? $data['locationName'] : null;
        $this->locationAddress = (isset($data['locationAddress'])) ? $data['locationAddress'] : null;
        $this->locationAddress2 = (isset($data['locationAddress2'])) ? $data['locationAddress2'] : null;
        $this->locationCity = (isset($data['locationCity'])) ? $data['locationCity'] : null;
        $this->locationState = (isset($data['locationState'])) ? $data['locationState'] : null;
        $this->locationPostalCode = (isset($data['locationPostalCode'])) ? $data['locationPostalCode'] : null;

        $this->shippingMethodName = (isset($data['shippingMethodName'])) ? $data['shippingMethodName'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
} 