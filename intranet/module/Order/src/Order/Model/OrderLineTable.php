<?php
namespace Order\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class OrderLineTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getOrderLines($orderId)
    {
        $select = new Select();
        $select->from('order_lines')
            ->columns(
                array(
                    'id',
                    'productId',
                    'serialIn',
                    'serialOut',
                    'problemReported',
                    'problemFound',
                    'workPreformed',
                    'receivedDate',
                    'reportedDoa',
                    'foundDoa',
                    'nri',
                    'status',
                    'shippedDate',
                    'shippingMethodOut',
                    'shippingMethodIn',
                    'trackingNumberOut',
                    'trackingNumberIn'
                )
            )
            ->join('products',
                'products.id = order_lines.productId',
                array(
                    'partNumber',
                    'alternativePartNumber',
                    'productDescription' => 'description'
                ),
                Select::JOIN_LEFT
            )
            ->where(array('order_lines.orderId' => $orderId));

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function getGroupedOrderLines ($orderId)
    {
        $select = new Select();
        $select->from('order_lines')
            ->columns(
                array(
                    'quantity' => new Expression('count(order_lines.productId)'),
                )
            )
            ->join('products',
                'products.id = order_lines.productId',
                array(
                    'partNumber',
                    'alternativePartNumber',
                    'productDescription' => 'description'
                ),
                Select::JOIN_LEFT
            )
            ->where(array('order_lines.orderId' => $orderId))
            ->group('productId');

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function getOrderLine($id = null)
    {
        $id = (int)$id;
        $rowSet = $this->tableGateway->select(array('id' => $id));
        $row = $rowSet->current();
        if(!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveOrderLine(OrderLine $orderLine)
    {
        $data = array(
            'orderId' => $orderLine->orderId,
            'productId' => $orderLine->productId,
            'serialIn' => $orderLine->serialIn,
            'serialOut' => $orderLine->serialOut,
            'problemReported' => $orderLine->problemReported,
            'reportedDoa' => $orderLine->reportedDoa,
            'receivedDate' => $orderLine->receivedDate,
        );

        $id = (int)$orderLine->id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getOrderLine($id)) {
                $updateData = array(
                    'problemFound' => $orderLine->problemFound,
                    'workPreformed' => $orderLine->workPreformed,
                    'foundDoa' => $orderLine->foundDoa,
                    'nri' => $orderLine->nri,
                    'status' => $orderLine->status,
                    'shippedDate' => $orderLine->shippedDate,
                    'shippingMethodOut' => $orderLine->shippingMethodOut,
                    'shippingMethodIn' => $orderLine->shippingMethodIn,
                    'trackingNumberOut' => $orderLine->trackingNumberOut,
                    'trackingNumberIn' => $orderLine->trackingNumberIn
                );

                $data = array_merge($data, $updateData);
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception("Form id does not exist");
            }
        }
    }

    public function removeOrderLinesByPartNumber($orderId, $productId)
    {

    }
} 