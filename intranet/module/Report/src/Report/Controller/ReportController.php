<?php
namespace Report\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractActionController
{

    public function indexAction()
    {
        return new ViewModel();
    }

    public function daysInRepairAction()
    {
        return new ViewModel();
    }

    public function missingInActionAction()
    {
        return new ViewModel();
    }

    public function noTroubleFoundAction()
    {
        return new ViewModel();
    }

    public function nonRepairableItemAction()
    {
        return new ViewModel();
    }

    public function workInProgressAction()
    {
        return new ViewModel();
    }

    public function deadOnArrivalAction()
    {
        return new ViewModel();
    }

    public function endOfLineAction()
    {
        return new ViewModel();
    }
} 