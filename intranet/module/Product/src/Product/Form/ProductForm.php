<?php

namespace Product\Form;

use Zend\Form\Form;

class ProductForm extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('product');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'partNumber',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Part Number',
            ),
        ));

        $this->add(array(
            'name' => 'alternativePartNumber',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Alternative Part Number',
            ),
        ));

        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
} 