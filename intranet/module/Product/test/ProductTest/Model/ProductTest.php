<?php
namespace ProductTest\Model;

use Product\Model\Product;
use PHPUnit_Framework_TestCase;

class ProductTest extends PHPUnit_Framework_TestCase
{
    public function testProductInitialState()
    {
        $product = new Product();

        $this->assertNull($product->partNumber, '"partNumber" should initially be null');
        $this->assertNull($product->id, '"id" should initially be null');
        $this->assertNull($product->alternativePartNumber, '"alternativePartNumber" should initially be null');
        $this->assertNull($product->description, '"description" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $product = new Product();
        $data = array(
            'productNumber' => 'PDT-3100',
            'id' => 123,
            'alternativeProductNumber' => 'PDT-3100',
            'description' => 'some description'
        );

        $product->exchangeArray($data);

        $this->assertSame($data['partNumber'], $product->partNumber, '"partNumber" was not set correctly');
        $this->assertSame($data['id'], $product->id, '"id" was not set correctly');
        $this->assertSame($data['alternativePartNumber'], $product->alternativePartNumber, '"alternativePartNumber" was not set correctly');
        $this->assertSame($data['description'], $product->description, '"description" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $product = new Product();

        $product->exchangeArray(array(
            'productNumber' => 'PDT-3100',
            'id' => 123,
            'alternativeProductNumber' => 'PDT-3100',
            'description' => 'some description'
        ));
        $product->exchangeArray(array());

        $this->assertNull($product->partNumber, '"partNumber" should have defaulted to null');
        $this->assertNull($product->id, '"id" should have defaulted to null');
        $this->assertNull($product->alternativePartNumber, '"alternativePartNumber" should have defaulted to null');
        $this->assertNull($product->description, '"description" should have defaulted to null');
    }
} 