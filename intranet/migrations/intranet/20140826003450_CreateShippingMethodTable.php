<?php

class CreateShippingMethodTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $table = $this->create_table('shipping_methods');
        $table->column('method', 'string',array('limit' => 100));
        $table->finish();
    }//up()

    public function down()
    {
        $this->drop_table('shipping_methods');
    }//down()
}
