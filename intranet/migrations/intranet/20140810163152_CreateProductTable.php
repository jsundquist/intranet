<?php

class CreateProductTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $table = $this->create_table('products');

        $table->column('partNumber','string', array('limit' => 100));
        $table->column('alternativePartNumber','string', array('limit' => 100));
        $table->column('description','string', array('limit' => 250));

        $table->finish();

    }//up()

    public function down()
    {
        $this->drop_table('products');
    }//down()
}
