<?php

class InsertShippingMethods extends Ruckusing_Migration_Base
{
    public function up()
    {
        // UPS Shipping Methods
        $this->execute('INSERT INTO shipping_methods (method) VALUES ("UPS Next Day Air Early A.M."),("UPS Next Day Air"), ("UPS Next Day Air Saver"), ("UPS 2nd Day Air A.M."), ("UPS 2nd Day Air"), ("UPS 3 Day Select"), ("UPS Ground")');

        // FedEx Shipping Methods
        $this->execute('INSERT INTO shipping_methods (method) VALUES ("FedEx First Overnight"), ("FexEx Priority Overnight"), ("FedEx Standard Overnight"), ("FedEx 2Day"), ("FedEx Express Saver"), ("FedExG Ground")');
    }//up()

    public function down()
    {
    }//down()
}
