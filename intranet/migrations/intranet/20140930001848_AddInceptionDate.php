<?php

class AddInceptionDate extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->add_column('order_lines', 'inceptionDate', 'date');
    }//up()

    public function down()
    {
        $this->remove_column('order_lines','inceptionDate');
    }//down()
}
