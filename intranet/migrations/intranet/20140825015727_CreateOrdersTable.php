<?php

class CreateOrdersTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $table = $this->create_table('orders');

        $table->column('type', 'string', array('limit' => 100));
        $table->column('locationId', 'integer');
        $table->column('shippingMethodId', 'integer');
        $table->column('contact', 'string', array('limit' => 100));
        $table->column('customerPurchaseOrder', 'string', array('limit' => 100));
        $table->column('created', 'datetime');

        $table->finish();
    }

    //up()

    public function down()
    {
        $this->drop_table('orders');
    }
    //down()
}
