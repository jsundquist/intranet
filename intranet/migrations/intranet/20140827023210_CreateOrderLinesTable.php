<?php

class CreateOrderLinesTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $table = $this->create_table('order_lines');
        $table->column('orderId', 'integer');
        $table->column('productId', 'integer');
        $table->column('serialIn', 'string', array('limit' => 100));
        $table->column('serialOut', 'string', array('limit' => 100));
        $table->column('problemReported', 'string', array('limit' => 150));
        $table->column('problemFound', 'string', array('limit' => 150));
        $table->column('workPreformed', 'string', array('limit' => 150));
        $table->column('receivedDate', 'date');
        $table->column('reportedDoa','tinyinteger');
        $table->column('foundDoa', 'tinyinteger');
        $table->column('nri','tinyinteger');
        $table->column('status','string');

        $table->column('shippedDate', 'date');
        $table->column('shippingMethodOut','integer');
        $table->column('trackingNumberOut','string', array('limit' => 100));
        $table->column('shippingMethodIn','integer');
        $table->column('trackingNumberIn','string', array('limit' => 100));

        $table->finish();
    }

    //up()

    public function down()
    {
        $this->drop_table('order_lines');
    }
    //down()
}
