<?php

class CreateCustomersTable extends Ruckusing_Migration_Base
{
    public function up()
    {
        $table = $this->create_table('customers');

        $table->column('name', 'string', array('limit' => 100));
        $table->column('address', 'string', array('limit' => 100));
        $table->column('address2', 'string', array('limit' => 100));
        $table->column('city', 'string', array('limit' => 100));
        $table->column('state', 'string', array('limit' => 100));
        $table->column('postalCode', 'string', array('limit' => 100));
        $table->column('country', 'string', array('limit' => 100));
        $table->column('phone', 'string', array('limit' => 100));
        $table->column('fax', 'string', array('limit' => 100));

        $table->finish();
    }

    //up()

    public function down()
    {
        $this->drop_table('customers');
    }
    //down()
}
